# IUS Auto DevOps

Instructions for utilizing the IUS Auto DevOps scripts.

## Table of contents

- [Prerequisites](#Prerequisites)
- [Set up kubernetes](#Set-up-kubernetes)
- [Set up runners](#Set-runners)
- [Prerequisites](#Prerequisites)
- [Create Repository](#Create-Repository)
- [The CI File](#The-CI-File)
    - [Pipeline Configuration](#Pipeline-Configuration)
    - [Advanced Configuration](#Advanced-Configuration)
    - [Docker Configuration](#Docker-Configuration)
    - [Environment Configurations](#Environment-Configurations)
        - [Code Configurations](#Code-Configurations)
        - [File Configurations](#File-Configurations)
        - [Gitlab variables](#Gitlab-Variables)
- [Framework Specific Settings](#Framework-Specific-Settings)
- [Gitlab](#Gitlab)
    - [Protected Branches](#Protected-Branches)
- [Code Coverage](#Code-Coverage)
    - [React](#React)
    - [Java Spring Boot](#Java-Spring-Boot)
- [Metrics](#Metrics)
- [Error Tracking](#Error-Tracking)
- [Visualization](#Visualization)

## Prerequisites

- Kubernetes cluster in AWS.

## Set up kubernetes

> **Note**: With the free plan of GitLab you're only able to add one kubernetes cluster. If you wish to add more than one to seperate environments by cluster you need to upgrade your plan.

You're able to set up the kluster on individual repositories, but we recommend setting it up for a group so all projects in the same group can utilize it. We're going to assume you're setting it up for a group.

After creating a group you'll find the set up process under the `Kubernetes` tab. Follow their instructions.

When the cluster is set up for your group you need to create an access token with read priviliges for the registry. This because the default token only allows access for 5 minutes. If a pod then fails and needs to be refetched it won't be able to do that since the token might have expired. The pod will then get stuck in a `CrashLoopBackOff`.

To create the access token go to your profile `Settings ➔ Access Tokens` and create a new token with access to `read_repository` and `read_registry` that never expires.

> **Note**: It's recommended to create the access token on a stand alone user. This so the functionality shouldn't be dependent on your user.

When the token is set up go to `Settings ➔ CI/CD ➔ Variables` and add the credentials to your user as `CUSTOM_CI_USERNAME`, `CUSTOM_CI_PASSWORD` and `CUSTOM_CI_EMAIL`.

The last variable that needs to be set up is `KUBE_INGRESS_BASE_DOMAIN`. This variables specifies which base domain the sub domains, that are used later, should be attach to. Note that you also need to set up so that domain points to the cluster that is used.

## Set up runners

By default there are `shared runners` in GitLab that can be utilized. Unfortunately they're limited to 2000 minutes of pipeline time per month. Using these might initially be a good solution if you're not handling a lot of projects, but we recommend setting up your own runners to not be dependant on GitLab's shared ones.

There are two way in achieving this. The first one being setting up it by following [GitLab's instructions](https://docs.gitlab.com/ee/ci/runners/). The second one being to use an auto scaled solution in the cluster. This can be achieved by following [this guide](https://docs.gitlab.com/runner/configuration/runner_autoscale_aws/).

## Create Repository

Create a new project, if you're working in a team you might need to create a subgroup under the group you've added the cluster configuration.

When creating the project, you may choose to leave it blank (if you have existing source) or you can choose to create the project from one of the provided templates by clicking the `Create from template` tab.

## The CI File

Once you have your repositry and source ready, copy the contents of the [Auto-DevOps.gitlab-ci.yml](https://gitlab.com/iusinnovation/auto-devops/blob/master/Auto-DevOps.gitlab-ci.yml) file, put it in the root of your project and rename it to `.gitlab-ci.yml`.

### Pipeline Configuration

Open the file and change the variables according to your need.

When naming subdomains, use the convention `<environment>-<project_name>`, for example setting `DEV_SUB_DOMAIN` to `dev-my-project` will create the environment under `dev-my-project.<KUBE_INGRESS_BASE_DOMAIN>`.

| Variable | Description |
| ------ | ----------- |
| `DEV_SUB_DOMAIN`   | Subdomain for the `DEV` environment. |
| `TEST_SUB_DOMAIN` | Subdomain for the `TEST` environment. |
| `DEMO_SUB_DOMAIN`    | Subdomain for the `DEMO` environment. |
| `PRODUCTION_DOMAIN` | *(NOT IMPLEMENTED)* Full production domain (excluding protocol). |
| ------ | ----------- |
| `TEST_DISABLED` | Set to `"false"` to enable automatic unit testing. |
| `CODE_QUALITY_DISABLED` | Set to `"false"` to enable automatic [Code Quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html) testing. |
| `LICENSE_MANAGEMENT_DISABLED` | Set to `"false"` to enable automatic [License Management](https://docs.gitlab.com/ee/user/application_security/license_management). |
| `PERFORMANCE_DISABLED` | Set to `"false"` to enable automatic  [Performance Testing](https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html). |
| `SAST_DISABLED` | Set to `"false"` to enable automatic [Static Application Security Testing](https://docs.gitlab.com/ee/user/application_security/sast/). |
| `DEPENDENCY_SCANNING_DISABLED` | Set to `"false"` to enable automatic [Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/).  |
| `CONTAINER_SCANNING_DISABLED` | Set to `"false"` to enable automatic [Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/). |
| `DAST_DISABLED` | Set to `"false"` to enable automatic [Dynamic Application Security Testing](https://docs.gitlab.com/ee/user/application_security/dast/). |
| `REVIEW_DISABLED` | Set to `"false"` to enable [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/). |
| ------ | ----------- |
| `LIVENESSPROBE_PATH` | HTTP GET request path of your app for liveness probing. |
| `LIVENESSPROBE_INITIALDELAYSECONDS` | Time in seconds to wait before sending the first liveness probe. |
| `LIVENESSPROBE_TIMEOUTSECONDS` | Time in seconds a liveness probe is allowed to live before it's considered failed. |
| `LIVENESSPROBE_PERIOD_SECONDS` | Time in seconds between liveness probe attempts. |
| `LIVENESSPROBE_SUCCESS_THRESHOLD` | Amount of successful liveness probes required before your application is considered alive.  |
| `LIVENESSPROBE_FAILURE_THRESHOLD` | Amount of failed liveness probes required before your application is considered dead. |
| `READINESSPROBE_PATH` | HTTP GET request path of your app for readiness probing. |
| `READINESSPROBE_INITIALDELAYSECONDS` | Time in seconds to wait before sending the first readiness probe. |
| `READINESSPROBE_TIMEOUTSECONDS` | Time in seconds a readniess probe is allowed to live before it's considered failed. |
| `READINESSPROBE_PERIOD_SECONDS` | Time in seconds between readiness probe attempts. |
| `READINESSPROBE_SUCCESS_THRESHOLD` | Amount of successful readiness probes required before your application is considered ready. |
| `READINESSPROBE_FAILURE_THRESHOLD` | Amount of failed readiness probes required before your application is considered not ready. |
| ------ | ----------- |
| `KUBERNETES_VERSION` | Do not change |
| `HELM_VERSION` | Do not change |
| `DOCKER_DRIVER` | Do not change |
| `ROLLOUT_RESOURCE_TYPE` | Do not change |

### Advanced Configuration

The CI file includes all other configurations from the `auto-devops` repository.

If your pipeline requires changes to these files, you can add them to your project and change your CI file to import them locally from your project instead.

You can also override individual jobs and settings by simply recreating the configuration (or just redefining the value you need to change) at the bottom of your CI file.

If such a need arises, please consult with the contributors to make sure that the change is necessary and also attempt to add this change permanently to the auto devops repository for everyone to use.

When setting up CI_ENVIRONMENT_NAME in the applications environment settings, the following can be used in order to fetch the CI_ENVIRONMENT_NAME.

```
before_script:
  - |
      if [[ -f Dockerfile ]]; then
        sed -i 's/$CI_ENVIRONMENT_NAME/'"$CI_ENVIRONMENT_NAME"'/g' Dockerfile
      fi
  - |
      if [[ -f src/config/environment.js ]]; then
        sed -i 's/process.env.ENVIRONMENT/'\'"$CI_ENVIRONMENT_NAME"\''/g' src/config/environment.js
      fi
```

### Docker Configuration

By default Auto DevOps will automatically try to create a Docker image. Depending on the programming language and framework the build script will try to figure out how the Docker image should be created.

If the automatically created Docker image isn't created as desired Auto DevOps with automatically use the Dockerfile places in the project root directory. So for specific requirements this is a possible solution.

### Environment Configurations

When your application is built, you might need to provide different values to certain configurations depending on which environment the application is being built for (for example database credentials or API endpoints).

There are multiple ways to achieve this, it depends on what type of application you're deploying and what type of configuration you need to perform.

#### Code Configurations

One method is to use the environment variable: `CI_ENVIRONMENT_NAME`. This method is suitable for in-code configurations. This variable will be set to one of the following values depending on the environment being built:

- dev
- test
- demo
- production

For any other branch (Review App) the variable will be set to `review`, if you plan on using Review Apps you should provide a default configuration for such environments.

#### File Configurations

Another method is to override the CI script, this is suitable for entire configuration files.

Create a configuration file for every environment you plan to use and put them in a folder somewhere appropriate within your project.

Edit your CI file and add a `before_script` statement at the bottom of it, the following is a generic example:

    before_script:
      - cp configs/"$CI_ENVIRONMENT_NAME".config ./.config

For example on production, when building the app this will first copy a file from `configs/production.config` and put it in the root of your project under the name `.config`. Use this to automatically choose a suitable configuration file based on the environment.

You can also specify a file per environment by overriding each build stage, for example:

    build_dev:
      before_script:
        - cp configs/my-development-config ./.config

    build_master:
      before_script:
        - cp configs/my-production-config ./.config

#### Gitlab Variables

You can also expose environment variables to the runners building your application by navigating to `Settings ➔ CI / CD Settings` and expanding the `Variables` section in your Gitlab project. This is suitable for storing sensitive configurations that should not be exposed in files in your Gitlab repository.

These variables have a limitation, they cannot be set per environment (with a free Gitlab plan). There are however workarounds for this.

You can still store sensitive configurations here with prefixes, for example if you create a variable called `production_sensitive_token` and then populate it with a `ABCDEF`, you can use the following CI script to fetch it per environment:

    before_script:
      - variable="${CI_ENVIRONMENT_NAME}_sensitive_token" && export SENSITIVE_TOKEN=$(printf '%s\n' "${!variable}")

Note that we use `$CI_ENVIRONMENT_NAME` to extrapolate the prefix of the variable. This will populate the `SENSITIVE_TOKEN` environment variable with what's configured in Gitlab, in this case it will be `ABCDEF` since the environment being built is `production` (`$CI_ENVIRONMENT_NAME` is `production`).

Subsequently you can now create another variable in Gitlab called `dev_sensitive_token` and this variable will be used to populate `SENSITIVE_TOKEN` when the `dev` environment is being built.

Just as described in the File Configurations section, you can skip the extrapolation and just define the variable per environment:

    build_dev:
      before_script:
        - export SENSITIVE_TOKEN=${dev_sensitive_token}

    build_master:
      before_script:
        - export SENSITIVE_TOKEN=${production_sensitive_token}

You can also populate entire configuration files using this method by saving the contents of the file in a Gitlab variable and then simply echoing the contents of the variable to a file:

    - echo $SENSITIVE_TOKEN > ./.config

> **Note:** Multiline variables cannot be masked.

> **Note:** In all of the examples above, the environment variables will only be exposed when your application is being built, they will not be available to the application when it's running. If you need to expose environment variables to your application when it's running, prefix them with `K8S_SECRET_`. More information can be found [here](https://gitlab.com/help/ci/variables/README#auto-devops-environment-variables).

## Framework Specific Settings

### Spring Boot (Java)

When building and testing your application, the pipeline will default to Java 8.
You can change this by adding a file called `system.properties` to the root of your project.
Example contents for Java 11:

    java.runtime.version=11

## Gitlab

Commit the CI file to your project and push it to Gitlab, your first pipeline should now start.

The following branches you can manually configure the environment URI for by changing the CI variables:

 - dev
 - test
 - demo
 - stage
 - master (production)

If you commit code to one of these branches, a pipeline will be started automatically and a cloud environment will be deployed (if the pipeline succeeds) according to your sub domain variable configuration.

All other branches will be automatically deployed as Review Apps (if you have review apps enabled in you CI config).

All deployed environments can be viewed by navigating to your project in Gitlab and going to `Operations` ➔ `Environments`. Here, you can also teardown any environment (except production) by clicking the `Stop environment` button.


### Protected Branches

Permissions are fundamentally defined around the idea of having read or write permission to the repository and branches. To prevent people from messing with history or pushing code without review, there are _protected branches_.

By default, a protected branch does four simple things:

* it prevents its creation, if not already created, from everybody except users with Maintainer permission
* it prevents pushes from everybody except users with Maintainer permission
* it prevents anyone from force pushing to the branch
* it prevents anyone from deleting the branch

To protect a branch, you need to have at least Maintainer permission level. Note that the `master` branch is protected by default.

1. Navigate to your project’s `Settings ➔ Repository`
2. Scroll to find the **Protected branches** section.
3. From the **Branch** dropdown menu, select the branch you want to protect and click **Protect**.
4. Once done, the protected branch will appear in the “Protected branches” list.

> **Note**: For more information visit [GitLab documentation](https://docs.gitlab.com/ee/user/project/protected_branches.html).

## Code Coverage

Code coverage is a measure used to describe the degree to which the source code of a program is executed when a particular test suite runs. A program with high code coverage, measured as a percentage, has had more of its source code executed during testing, which suggests it has a lower chance of containing undetected software bugs compared to a program with low test coverage.

### React

Gettings code coverage to work in a React project requires two steps.

- Set up so code coverage is calculated when running tests. This is done by changing the value of `scripts ➔ test` in `package.json`. E.g. if you're using `jest` change the value to `jest --coverage`.
- Set up `regex` under `Settings ➔ CI/CD ➔ General pipelines` for that specific project.

Now when tests run and code coverage is created the repository will automatically add a badge containing coverage percentage.

### Java Spring Boot

Gettings code coverage to work in a Java Spring Boot project requires two steps.

- Set up `regex` under `Settings ➔ CI/CD ➔ General pipelines` for that specific project.
- Under `Test coverage parsing`, fill in the example for `JaCoCo (Java/Kotlin)`.

Edit your `build.gradle` file and add the following to it:

```
apply plugin: 'jacoco'

test {
    finalizedBy jacocoTestReport
}

jacocoTestReport {
    finalizedBy jacocoTestCoverageVerification
}

jacocoTestCoverageVerification {
    violationRules {
        rule {
            limit {
                minimum = 0.9
            }
        }
    }
}
```

Change the minimum code coverage limit if you wish, recommended is `0.9` (90%).

## Visualization

```mermaid
graph TD;
    HerokuBuildpacks[Heroku BuildPack]
    HerokuBuildpacks --> RecognizeProject[Recognize Project]

    RecognizeProject -.-> Ruby[Ruby]
    RecognizeProject -.-> Node[Node]
    RecognizeProject -.-> Clojure[Clojure]
    RecognizeProject -.-> Python[Python]
    RecognizeProject -.-> Java[Java]
    RecognizeProject -.-> Gradle[Gradle]
    RecognizeProject -.-> Grails[Grails 3.x]
    RecognizeProject -.-> versions[versions]
    RecognizeProject -.-> Scala[Scala]
    RecognizeProject -.-> Play[Play 2.x]
    RecognizeProject -.-> versions[versions]
    RecognizeProject -.-> PHP[PHP]
    RecognizeProject -.-> Go[Go]

    Build[Build Docker image]
    Ruby -.-> Build
    Node -.-> Build
    Clojure -.-> Build
    Python -.-> Build
    Java -.-> Build
    Gradle -.-> Build
    Grails -.-> Build
    versions -.-> Build
    Scala -.-> Build
    Play -.-> Build
    versions -.-> Build
    PHP -.-> Build
    Go -.-> Build

    Push[Push Docker image]
    Build --> Push
    Push --> Test[Test]
    Push --> CodeQuality[Code Quality]
    Push --> LicensManagement[Licens Management]
    Push --> Sast[SAST]
    Push --> ContainerScanning[Container Scanning]
    Push --> DependencyScanning[Dependency Scanning]

    Test --> Deploy
    CodeQuality --> Deploy
    LicensManagement --> Deploy
    Sast --> Deploy
    DependencyScanning --> Deploy

    Deploy --> DAST

    DAST --> Performance[Performance]

    Performance --> Done
```

### License
IUS Auto DevOps is MIT licensed.
